<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| RainTemplate Config File
| -------------------------------------------------------------------------
| 
*/

// Is the directory where all templates are stored
$config['tpl_dir'] 					= APPPATH . 'views/';


// Is the directory where RainTPL compile the templates
$config['cache_dir'] 				= APPPATH . 'cache/';


// Is the templates extension
$config['tpl_ext'] 					= 'html';


// RainTPL when compile a template, replaces all relative paths with the correct server path.
$config['path_replace'] 			= TRUE;


// Is possible to configure what elements to parse.
// You can set only the following elements, <a>, <img>, <link> and <script>.
$config['path_replace_list'] 		= array('a', 'img', 'link', 'script');


// It define what functions and variables cannot be used into the template.
$config['black_list'] 				= array();


// Set it false only if your cache directory has not write permission,
// else is strongly advised to re-compile the template for each change.
$config['check_template_update']	= TRUE;


// Set true if you want to enable the use of PHP codes in your templates.
// Its strongly recommend to keep this configuration disabled.
$config['php_enabled'] 				= FALSE;